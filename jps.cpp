#include "jps.h"

JPS::~JPS(void) {}

inline int vertex(Cell item, int size) {
    return item.x * size + item.y;
}

double JPS::get_cost(Cell from, Cell to, double linecost) const {
    return linecost * euclid_heuristic(from, to);
}

Cell JPS::jump(Node current) {
    int x, y;
    if(current.point.y < current.parent->point.y) {
        y = -1;
    } else if(current.point.y > current.parent->point.y) {
        y = 1;
    } else {
        y = 0;
    }
    if(current.point.x < current.parent->point.x) {
        x = -1;
    } else if(current.point.x > current.parent->point.x) {
        x = 1;
    } else {
        x = 0;
    }
    return Cell(x, y);
}

void JPS::get_if_cutcorner(Cell move, Node node, Node *parent, const Graph &map) {
    if(move.y == 0) {
        if(map.in_bounds(Cell(node.point, 0, -move.x))) {
            if(!map.passable(Cell(node.point, 0, -move.x))) {
                get_jump_points(Cell(move.x, -move.x), node, parent, map);
            }
        }
        if(map.in_bounds(Cell(node.point, 0, move.x))) {
            if(!map.passable(Cell(node.point, 0, move.x))) {
                get_jump_points(Cell(move.x, move.x), node, parent, map);
            }
        }
    }
    else if(move.x == 0) {
        if(map.in_bounds(Cell(node.point, -move.y, 0))) {
            if(!map.passable(Cell(node.point, -move.y, 0))) {
                get_jump_points(Cell(-move.y, move.y), node, parent, map);
            }
        }
        if(map.in_bounds(Cell(node.point, move.y, 0))) {
            if(!map.passable(Cell(node.point, move.y, 0))) {
                get_jump_points(Cell(move.y, move.y), node, parent, map);
            }
        }
    }
}

void JPS::get_if_not_cutcorner(Cell move, Node node, Node *parent, const Graph &map) {
    if(move.y == 0) {
        if(map.in_bounds(Cell(node.point, 0, -move.x))) {
            if(!map.passable(Cell(node.point, -move.x, -move.x))) {
                get_jump_points(Cell(move.x, -move.x), node, parent, map);
                get_jump_points(Cell(0, -move.x), node, parent, map);
            }
        }
        if(map.in_bounds(Cell(node.point, 0, move.x))) {
            if(!map.passable(Cell(node.point, -move.x, move.x))) {
                get_jump_points(Cell(0, move.x), node, parent, map);
                get_jump_points(Cell(move.x, move.x), node, parent, map);
            }
        }
    } else if(move.x == 0) {
        if(map.in_bounds(Cell(node.point, -move.y, 0))) {
            if(!map.passable(Cell(node.point, -move.y, -move.y))) {
                get_jump_points(Cell(-move.y, 0), node, parent, map);
                get_jump_points(Cell(-move.y, move.y), node, parent, map);
            }
        }
        if(map.in_bounds(Cell(node.point, move.y, 0))) {
            if(!map.passable(Cell(node.point, move.y, -move.y))) {
                get_jump_points(Cell(move.y, 0), node, parent, map);
                get_jump_points(Cell(move.y, move.y), node, parent, map);
            }
        }
    }
}


void JPS::get_successors(Node node, Node *parent, const Graph &map) {
    Cell move(0, 0);
    if(map.algorithm_info.allowdiagonal) {
        if(node.point == map.start_cell) {
            for(int n = -1; n <= 1; ++n) {
                for(int m = -1; m <= 1; ++m) {
                    if(n != 0 || m != 0) {
                        get_jump_points(Cell(m, n), node, parent, map);
                    }
                }
            }
        }
        if(node.point.y != map.start_cell.y || node.point.x != map.start_cell.x) {
            move = jump(node);
            get_jump_points(move, node, parent, map);
            if(move.x != 0 && move.y != 0) {
                if(!map.passable(Cell(node.point, 0, -move.y)))
                    get_jump_points(Cell(move.x, -move.y), node, parent, map);
                if(!map.passable(Cell(node.point, -move.x, 0)))
                    get_jump_points(Cell(-move.x, move.y), node, parent, map);
                get_jump_points(Cell(0, move.y), node, parent, map);
                get_jump_points(Cell(move.x, 0), node, parent, map);
            }

            if(map.algorithm_info.cutcorners) {
                get_if_cutcorner(move, node, parent, map);
            } else {
                get_if_not_cutcorner(move, node, parent, map);
            }
        }
    } else {
        if(node.point == map.start_cell)
            for(int n = -1; n <= 1; n++)
                for(int m = -1; m <= 1; m++)
                    if((n != 0 && m == 0) || (n == 0 && m != 0))
                        get_jump_points(Cell(m, n), node, parent, map);
        if(node.point.y != map.start_cell.y || node.point.x != map.start_cell.x) {
            move = jump(node);

            get_jump_points(move, node, parent, map);
            get_jump_points(Cell(move.y, move.x), node, parent, map);
            get_jump_points(Cell(-move.y, -move.x), node, parent, map);
        }
    }
    return;
}

bool JPS::neighbor(Cell move, Node node, const Graph &map) {
    while(map.in_bounds(node.point) && map.passable(node.point)) {
        if(node.point == map.finish_cell) return true;
        if(map.algorithm_info.cutcorners) {
            if(move.y == 0 && map.in_bounds(Cell(node.point, move.x, 0))) {
                if(map.in_bounds(Cell(node.point, 0, 1))) {
                    if(map.passable(Cell(node.point, move.x, 1)) && !map.passable(Cell(node.point, 0, 1))) {
                        return true;
                    }
                }
                if(map.in_bounds(Cell(node.point, 0, -1))) {
                    if(map.passable(Cell(node.point, move.x, -1)) && !map.passable(Cell(node.point, 0, -1))) {
                        return true;
                    }
                }
            }
            if(move.x == 0 && map.in_bounds(Cell(node.point, 0, move.y))) {
                if(map.in_bounds(Cell(node.point, 1, 0))) {
                    if(map.passable(Cell(node.point, 1, move.y)) && !map.passable(Cell(node.point, 1, 0))) {
                        return true;
                    }
                }
                if(map.in_bounds(Cell(node.point, -1, 0))) {
                    if(map.passable(Cell(node.point, -1, move.y)) && !map.passable(Cell(node.point, -1, 0))) {
                        return true;
                    }
                }
            }
        }
        else {
            if(move.y == 0 && map.in_bounds(Cell(node.point, -move.x, 0))) {
                if(map.in_bounds(Cell(node.point, 0, 1))) {
                    if(map.passable(Cell(node.point, 0, 1)) && !map.passable(Cell(node.point, -move.x, 1))) {
                        return true;
                    }
                }
                if(map.in_bounds(Cell(node.point, 0, -1))) {
                    if(map.passable(Cell(node.point, 0, -1)) && !map.passable(Cell(node.point, -move.x, -1))) {
                        return true;
                    }
                }
            }
            if(move.x == 0 && map.in_bounds(Cell(node.point, 0, -move.y))) {
                if(map.in_bounds(Cell(node.point, 1, 0))) {
                    if(map.passable(Cell(node.point, 1, 0)) && !map.passable(Cell(node.point, 1, -move.y))) {
                        return true;
                    }
                }
                if(map.in_bounds(Cell(node.point, -1, 0))) {
                    if(map.passable(Cell(node.point, -1, 0)) && !map.passable(Cell(node.point, -1, -move.y))) {
                        return true;
                    }
                }
            }
        }
        node.point += move;
    }
    return false;
}

void JPS::jump_if_not_cutcorner(bool &exist, Cell move, Node node, const Graph& map) {
    if(move.y == 0) {
        if(map.in_bounds(Cell(node.point, 0, 1))) {
            if(map.passable(Cell(node.point, 0, 1)) && !map.passable(Cell(node.point, -move.x, 1))) {
                exist = true;
            }
        }
        if(map.in_bounds(Cell(node.point, 0, -1))) {
            if(map.passable(Cell(node.point, 0, -1)) && !map.passable(Cell(node.point, -move.x, -1))) {
                exist = true;
            }
        }
    } else if(move.x == 0) {
        if(map.in_bounds(Cell(node.point, 1, 0))) {
            if(map.passable(Cell(node.point, 1, 0)) && !map.passable(Cell(node.point, 1, -move.y))) {
                exist = true;
            }
        }
        if(map.in_bounds(Cell(node.point, -1, 0))) {
            if(map.passable(Cell(node.point, -1, 0)) && !map.passable(Cell(node.point, -1, -move.y))) {
                exist = true;
            }
        }
    } else {
        if(neighbor(Cell(0, move.y), node, map)) {
            exist = true;
        }
        if(!exist) {
            if(neighbor(Cell(move.x, 0), node, map)) {
                exist = true;
            }
        }
    }
}

void JPS::jump_if_cutcorner(bool &exist, Cell move, Node node, const Graph& map) {
    if(move.y == 0) {
        if(map.in_bounds(Cell(node.point, move.x, 1))) {
            if(map.passable(Cell(node.point, move.x, 1)) && !map.passable(Cell(node.point, 0, 1))) {
                exist = true;
            }
        }
        if(map.in_bounds(Cell(node.point, move.x, -1))) {
            if(map.passable(Cell(node.point, move.x, -1)) && !map.passable(Cell(node.point, 0, -1))) {
                exist = true;
            }
        }
    }
    else if(move.x == 0) {
        if(map.in_bounds(Cell(node.point, 1, move.y))) {
            if(map.passable(Cell(node.point, 1, move.y)) && !map.passable(Cell(node.point, 1, 0))) {
                exist = true;
            }
        }
        if(map.in_bounds(Cell(node.point, -1, move.y))) {
            if(map.passable(Cell(node.point, -1, move.y)) && !map.passable(Cell(node.point, -1, 0))) {
                exist = true;
            }
        }
    } else {
        if(map.in_bounds(Cell(node.point, move.x, -move.y))) {
            if(!map.passable(Cell(node.point, 0, -move.y)) && map.passable(Cell(node.point, move.x, -move.y))) {
                exist = true;
            }
        }
        if(!exist && map.in_bounds(Cell(node.point, -move.x, move.y))) {
            if(!map.passable(Cell(node.point, -move.x, 0)) && map.passable(Cell(node.point, -move.x, move.y))) {
                exist = true;
            }
        }
        if(!exist) {
            if(neighbor(Cell(0, move.y), node, map)) {
                exist = true;
            }
        }
        if(!exist) {
            if(neighbor(Cell(move.x, 0), node, map)) {
                exist = true;
            }
        }
    }
}

void JPS::get_jump_points(Cell move, Node node, Node *parent, const Graph &map) {
    bool exist = false;
    while(!exist) {
        if(map.in_bounds(node.point + move)) {
            if(!map.algorithm_info.cutcorners) {
                if(move.x != 0 && move.y != 0)
                    if(!map.passable(Cell(node.point, move.x, 0)) || !map.passable(Cell(node.point, 0, move.y)))
                        return;
            } else if(!map.algorithm_info.allowsqueeze) {
               if(move.y != 0 && move.x != 0)
                   if(!map.passable(Cell(node.point, move.x, 0)) && !map.passable(Cell(node.point, 0, move.y)))
                       return;
            }
            if(map.passable(node.point + move)) {
                node.g += get_cost(node.point, node.point + move, map.algorithm_info.linecost);
                node.point += move;
            } else return;
        } else return;

        if(node.point == map.finish_cell) exist = true;
        if(map.algorithm_info.allowdiagonal) {
            if(map.algorithm_info.cutcorners) {
                jump_if_cutcorner(exist, move, node, map);
            } else {
                jump_if_not_cutcorner(exist, move, node, map);
            }
        } else {
            if(!exist) {
                if(neighbor(Cell(move.y, move.x), node, map)) {
                    exist = true;
                }
            }
            if(!exist) {
                if(neighbor(Cell(-move.y, -move.x), node, map)) {
                    exist = true;
                }
            }
        }
    }

    if(!CLOSE.count(vertex(node.point, map.height))) {
        node.parent = parent;
        node.h = heuristic(node.point, map.finish_cell, map);
        node.f = node.g + map.algorithm_info.hweight * node.h;
        OPEN.put(node, map.algorithm_info.breakingties);
    }
    return;
}


void JPS::get_primary_path(Path &path, Node goal) {
    std::vector<Section> result;
    while(goal.parent != nullptr) {
        result.push_back(Section(*goal.parent, goal));
        goal = *goal.parent;
    }
    reverse(result.begin(), result.end());
    path.path_hp = result;
}

void JPS::get_secondary_path(Path &path) {
    std::vector<Node> result;
    for (auto elem : path.path_hp) {
        for (auto point : lineBresenham(elem.start.point, elem.finish.point)) {
            result.push_back(Node(point));
        }
    }
    path.path_lp = result;
}
