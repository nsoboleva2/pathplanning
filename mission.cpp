#include "mission.h"


Mission::Mission() {
    search = nullptr;
}
Mission::~Mission() { if (search) delete search; }

void Mission::create_search(const Algorithm &al_info) {
    if (search) delete search;
    else if (al_info.searchtype.find(CNS_ASTAR) != std::string::npos) {
       search = new Astar();
    }
    else if (al_info.searchtype.find(CNS_THETA) != std::string::npos) {
        search = new Theta();
    }
    else if (al_info.searchtype.find(CNS_JPS) != std::string::npos) {
        search = new JPS();
    }

}

Result Mission::start_search(const Graph &map, const Algorithm &al_info) {
    create_search(al_info);
    search_result = search->find_path(map, al_info);
    return search_result;
}

void Mission::print_info() {
    if (!search_result.path_existance) {
        std::cout << "PATH_NOT_FOUND";
        return;
    }
    std::cout << "PATH FOUND" << std::endl;
    std::cout << "number of steps: " << search_result.steps << std::endl;
    std::cout << "nodes created: " << search_result.nodes << std::endl;
    std::cout << "length:" << search_result.length << std::endl;
    std::cout << "time:" << search_result.time << std::endl;
    std::cout << "Path: " << std::endl;
    if (search_result.steps < 500) {
        for (auto elem : search_result.path.path_lp) {
            std::cout << elem ;
        }
        std::cout << std::endl;
    }
}


