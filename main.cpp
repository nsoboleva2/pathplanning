#include <iostream>
#include "mission.h"

int main(int argc, char *argv[])
{
    try {
        if(!argv[1]) {
            throw "No input file. Please try again";
        }
    }
    catch (const char *exc) {
        std::cout << exc << std::endl;
        return 0;
    }
    char *file_name = argv[1];
    Grid graph(file_name);
    //Astar path;
    //Result res = path.find_path(graph, graph.algorithm_info);
    Mission path;
    Result res = path.start_search(graph, graph.algorithm_info);
    path.print_info();
    //std::cout << std::endl;
    //out_path(path, graph);
    create_xml<Grid, Result>(graph, res);
    return 0;
}

