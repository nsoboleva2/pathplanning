#ifndef OPENLIST_H
#define OPENLIST_H

#include "node.h"
#include "gl.const.h"
#include <list>
#include <vector>

class OpenList {
public:

    inline bool empty() const {
        return !open_size;
    }

    inline size_t size_l() const {
        return open_size;
    }

    inline Node get(std::string break_tie) {
        Node best;
        best.f = std::numeric_limits<double>::infinity();
        int coord = 0;
        for (size_t i = 0; i < elements.size(); ++i) {
            if (!elements[i].empty()) {
                if (elements[i].front().f < best.f) {
                    best = elements[i].front();
                    coord = i;
                } else if (elements[i].front().f == best.f) {
                    if ((break_tie == CNS_GMAX && (elements[i].front().g >= best.g)) ||
                        (break_tie == CNS_GMIN && (elements[i].front().g <= best.g))) {
                        best = elements[i].front();
                        coord = i;
                    }
                }
            }
        }
        elements[coord].pop_front();
        --open_size;
        return best;
    }

    inline void put(Node item, std::string break_tie) {
        bool exist = false;
        std::list<Node>::iterator it = elements[item.point.x].begin();
        std::list<Node>::iterator position = elements[item.point.x].end();
        if (elements[item.point.x].size() == 0) {
            elements[item.point.x].emplace_back(item);
            ++open_size;
            return;
        }
        while (it != elements[item.point.x].end()) {
            if (it->point == item.point) {
                exist = true;
                if (it->f > item.f) {
                    it->f = item.f;
                    it->g = item.g;
                    it->parent = item.parent;
                }
                else if (it->f == item.f &&
                        ((break_tie == CNS_GMAX && (it->g >= item.g)) ||
                        (break_tie == CNS_GMIN && (it->g <= item.g)))) {
                    it->g = item.g;
                    it->parent = item.parent;
                }
                break;
            }
            if (it->f < item.f) {
                position = it;
            } else if(it->f == item.f &&
                     ((break_tie == CNS_GMAX && (it->g < item.g)) ||
                     (break_tie == CNS_GMIN && (it->g > item.g)))) {
                position = it;
            }
            ++it;
        }
        if(!exist) {
            if (++position != elements[item.point.x].end()) elements[item.point.x].emplace(position, item);
            else elements[item.point.x].emplace_back(item);
            ++open_size;
        }
    }

    inline void resize(int value) {
        elements.resize(value);
    }

    std::vector<Node> get_elements() const {
        std::vector<Node> result;
        for (auto elem : elements) {
            for(auto it = elem.begin(); it != elem.end(); ++it) {
                result.push_back(*it);
            }
        }
        return result;
    }

    std::vector<std::list<Node>> elements;
    size_t open_size;

};

#endif // OPENLIST_H
