#ifndef THETA_H
#define THETA_H

#include "astar.h"
#include "bresenham.h"

class Theta : public Astar {
public:

    Theta() : Astar() {}
    ~Theta(void);

protected:

    double get_cost(const Node &from, const Node &to, double linecost) const;
    Node reset_parent(Node &n, const Graph &map);
    void get_primary_path(Path &path, Node goal);
    void get_secondary_path(Path &path);
};


#endif // THETA_H
