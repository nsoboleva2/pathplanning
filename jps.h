#ifndef JPS_H
#define JPS_H

#include <ctime>
#include <cassert>
#include "openlist.h"
#include "heuristics.h"
#include "grid.h"
#include "astar.h"
#include "bresenham.h"

typedef Grid Graph;

class JPS : public Astar
{
public:
    JPS() {}
    ~JPS(void);

private:

    double get_cost(Cell from, Cell to, double linecost) const;
    bool neighbor(Cell move, Node cur, const Graph &map);
    void get_jump_points(Cell move, Node cur, Node *parent, const Graph &map);
    void check_for_jump(Cell c1, Cell c2, Cell move, Node *node, const Graph &map);
    Cell jump(Node current);

    void get_if_cutcorner(Cell move, Node node, Node *parent, const Graph &map);
    void get_if_not_cutcorner(Cell move, Node node, Node *parent, const Graph &map);
    void jump_if_cutcorner(bool &exist, Cell move, Node node, const Graph &map);
    void jump_if_not_cutcorner(bool &exist, Cell move, Node node, const Graph &map);

    void get_successors(Node node, Node *parent, const Graph &map);
    void get_primary_path(Path &path, Node goal);
    void get_secondary_path(Path &path);

};


#endif // JPS_H
