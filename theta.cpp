#include "theta.h"

Theta::~Theta(void) {}

double Theta::get_cost(const Node &from, const Node &to, double linecost) const {
    return linecost * euclid_heuristic(from.point,to.point);
}

Node Theta::reset_parent(Node &n, const Graph &map) {
    if (n.parent->parent == nullptr || n.parent == nullptr)
        return n;
    if(n == *n.parent->parent)
        return n;
    if (line_of_sight(n.parent->parent->point, n.point, map)) {
        n.g = n.parent->parent->g + get_cost(n.parent->parent->point, n.point, map.algorithm_info.linecost);
        n.parent = n.parent->parent;
        return n;
    }
    return n;
}

void Theta::get_primary_path(Path &path, Node goal) {
    std::vector<Section> result;
    while(goal.parent != nullptr) {
        result.push_back(Section(*goal.parent, goal));
        goal = *goal.parent;
    }
    reverse(result.begin(), result.end());
    path.path_hp = result;
}

void Theta::get_secondary_path(Path &path) {
    std::vector<Node> result;
    for (auto elem : path.path_hp) {
        for (auto point : lineBresenham(elem.start.point, elem.finish.point)) {
            result.push_back(Node(point));
        }
    }
    path.path_lp = result;
}

