#ifndef GRID_H
#define GRID_H

#include "parser.h"
#include "node.h"
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <cmath>

class Grid {

private:
    int ** grid;

public:
    int cell_size;

    const char *title;
    const char *filename;
    Algorithm algorithm_info;

    Cell start_cell;
    Cell finish_cell;
    int width, height;

    Grid();
    Grid(int width_, int height_, int cell_size_);
    Grid(char * filename);

    ~Grid();

    int * operator [] (int i);
    const int * operator [] (int i) const;
    bool in_bounds(Cell current) const;
    bool passable(Cell current) const;
    bool not_passable(Cell current) const;
    bool squeeze(Cell next, Cell current) const;
    bool cut(Cell next, Cell current) const;
    bool is_neighbor(Cell next, Cell current) const;


    void build_grid();

    std::vector<Cell> neighbors(Cell current) const;

    double cost(Cell a, Cell b) const;
    friend inline std::ostream& operator<< (std::ostream& out, const Grid &next) {
        for (int i = 0; i < next.height; ++i) {
            for (int j = 0; j < next.width; ++j) {
              out << next[i][j] << " ";
            }
            out << "\n";
        }

        return out;
    }
};

#endif // GRID_H
