#ifndef MISSION_H
#define MISSION_H

#include "grid.h"
#include "searcher.h"
#include "astar.h"
#include "theta.h"
#include "structures.h"
#include "gl.const.h"
#include "jps.h"

typedef Grid Graph;

class Mission {
    public:
        Mission();
        ~Mission();

        void create_search(const Algorithm &al_info);
        Result start_search(const Graph &map, const Algorithm &al_info);
        void print_info();

    private:
        Searcher *search;
        Result search_result;
};

#endif // MISSION_H
