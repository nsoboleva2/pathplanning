#ifndef SEARCHER_H
#define SEARCHER_H

#include "grid.h"
#include "openlist.h"
#include "gl.const.h"
#include "heuristics.h"
#include "structures.h"
#include <ctime>
#include <unordered_map>

typedef Grid Graph;

class Searcher
{
public:
    Searcher();
    virtual ~Searcher(void);

    Result find_path(const Graph &map, const Algorithm &info);

protected:
    void start_search(Algorithm info);
    virtual Node reset_parent(Node &n, const Graph &map);
    virtual void get_successors(Node n, Node *parent, const Graph &map);
    std::vector<Node> find_neighbors(Node n, Node *parent, const Graph &map) const;
    double get_cost(Node from, Node to, double linecost) const;

    virtual void get_primary_path(Path &path, Node goal);
    virtual void get_secondary_path(Path &path);
    Path reconstruct_path(Node goal);

    Result search_result;
    OpenList OPEN;
    std::unordered_map<int, Node> CLOSE;
};

#endif // SEARCHER_H
