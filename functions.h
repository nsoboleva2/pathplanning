#ifndef FUNCTIONS
#define FUNCTIONS
#include <iostream>
#include <sstream>
#include "grid.h"

template<typename T>
std::ostream& operator << (std::ostream&out, const Grid<T>& A) {
    out << "Grid:\n";
    out << "Start: (" << A.start_cell.x << "," << A.start_cell.y << ")\n";
    out << "Goal: (" << A.finish_cell.x << "," << A.finish_cell.y << ")\n";
    for (int i = 0; i != A.height; ++i){
        for (int k = 0; k != A.width; ++k)
            out << A[i][k] << " ";
        out << "\n";
    }
    return out;
}

void out_path(Astar &A, Grid<int> &B) {
    if (A.path_existance) {
        std::cout << "PATH:\n";
        for (auto next : A.path) {
            std::cout << "(" << next.point.x << "," << next.point.y << "); ";
        }
        std::cout << std::endl;
        Grid<int> C(B.width, B.height, B.cell_size);
        for (int i = 0; i != B.height; ++i) {
            for (int k = 0; k != B.width; ++k) {
                C[i][k] = B[i][k];
            }
        }
        for (auto next : A.path) {
            C[next.point.y][next.point.x] = 4;
        }
        for (int i = 0; i != C.height; ++i) {
            for (int k = 0; k != C.width; ++k) {
                if (C[i][k] == 4) {
                    std::cout << "* ";
                } else if (C[i][k] == 0){
                    std::cout << ". ";
                } else {
                    std::cout << "# ";
                }
            }
            std::cout << "\n";
        }
    }
}

std::ostream& operator<< (std::ostream& out, const Astar& A) {
    out << "Path:\n";
    for (auto next : A.path) {
        out << "(" << next.point.x << "," << next.point.y << "); ";
    }
    return out;
}



#endif // FUNCTIONS

