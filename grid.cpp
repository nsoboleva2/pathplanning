#include "grid.h"


Grid::Grid(): grid(NULL) {}
Grid::Grid(int width_, int height_, int cell_size_): width(width_), height(height_), cell_size(cell_size_) {
    build_grid();
}

Grid::Grid(char * filename) {
    get_map(*this, filename);
}
int * Grid::operator [] (int i) {
    return grid[i];
}
const int * Grid::operator [] (int i) const {
    return grid[i];
}
bool Grid::in_bounds(Cell current) const {
    return 0 <= current.x && current.x < width && 0 <= current.y && current.y < height;
}
bool Grid::passable(Cell current) const {
    return !grid[current.y][current.x];
}

bool Grid::not_passable(Cell current) const {
    return grid[current.y][current.x];
}

bool Grid::squeeze(Cell next, Cell current) const {
    if (next.x == current.x || next.y == current.y) return 1;
    if (algorithm_info.allowsqueeze) return 1;
    if (next.x + 1 == current.x && next.y - 1 == current.y)
        return !(grid[current.y][current.x - 1] && grid[current.y + 1][current.x]);
    if (next.x + 1 == current.x && next.y + 1 == current.y)
        return !(grid[current.y][current.x - 1] && grid[current.y - 1][current.x]);
    if (next.x - 1 == current.x && next.y + 1 == current.y)
        return !(grid[current.y][current.x + 1] && grid[current.y - 1][current.x]);
    if (next.x - 1 == current.x && next.y - 1 == current.y)
        return !(grid[current.y][current.x + 1] && grid[current.y + 1][current.x]);
    return 0;
}

bool Grid::cut(Cell next, Cell current) const {
    if (next.x == current.x || next.y == current.y) return 1;
    if (algorithm_info.cutcorners) return 1;
    if (next.x + 1 == current.x && next.y - 1 == current.y)
        return !(grid[current.y][current.x - 1] || grid[current.y + 1][current.x]);
    if (next.x + 1 == current.x && next.y + 1 == current.y)
        return !(grid[current.y][current.x - 1] || grid[current.y - 1][current.x]);
    if (next.x - 1 == current.x && next.y + 1 == current.y)
        return !(grid[current.y][current.x + 1] || grid[current.y - 1][current.x]);
    if (next.x - 1 == current.x && next.y - 1 == current.y)
        return !(grid[current.y][current.x + 1] || grid[current.y + 1][current.x]);
    return 0;
}

bool Grid::is_neighbor(Cell next, Cell current) const {
    return in_bounds(next) && passable(next) && squeeze(next, current) && cut(next, current);
}

Grid::~Grid() {
    if(grid) {
        for(int c = 0; c < height; c++) {
            delete [] grid[c];
        }
        delete[] grid;
    }
}

void Grid::build_grid() {
    grid = new int * [height];
    for(int count = 0; count < height; ++count){
        grid[count] = new int [width];
    }
}

std::vector<Cell> Grid::neighbors(Cell current) const {
    std::vector<Cell> results;
    if (algorithm_info.allowdiagonal) {
        for (int k = current.x - 1; k <= current.x + 1; ++k) {
            for (int l = current.y - 1; l <= current.y + 1; ++l) {
                if (!(k == current.x && l == current.y) && in_bounds(Cell(k, l))
                        && passable(Cell(k, l))
                           && squeeze(Cell(k, l), current)
                               && cut(Cell(k, l), current)){
                    results.push_back(Cell(k, l));
                }
            }
        }
    } else {
        for (int k = current.x - 1; k <= current.x + 1; ++k)
            if (k != current.x && in_bounds(Cell(k, current.y)) && passable(Cell(k, current.y)))
                results.push_back(Cell(k, current.y));
        for (int l = current.y - 1; l <= current.y + 1; ++l)
            if (l != current.y && in_bounds(Cell(current.x, l)) && passable(Cell(current.x, l)))
                results.push_back(Cell(current.x, l));
    }
    return results;
}

double Grid::cost(Cell a, Cell b) const{
    if(a.x == b.x || a.y == b.y) {
        return algorithm_info.linecost;
    }
    return algorithm_info.diagonalcost;
}
