TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt


win32 { QMAKE_LFLAGS += -static -static-libgcc -static-libstdc++ }

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -lgsl
QMAKE_CXXFLAGS += -lcblas


SOURCES += main.cpp \
    tinyxml/tinystr.cpp \
    tinyxml/tinyxml.cpp \
    tinyxml/tinyxmlerror.cpp \
    tinyxml/tinyxmlparser.cpp \
    grid.cpp \
    searcher.cpp \
    theta.cpp \
    mission.cpp \
    jps.cpp

HEADERS += \
    grid.h \
    gl.const.h \
    structures.h \
    parser.h \
    tinyxml/tinystr.h \
    tinyxml/tinyxml.h \
    heuristics.h \
    openlist.h \
    node.h \
    searcher.h \
    bresenham.h \
    astar.h \
    theta.h \
    mission.h \
    jps.h

