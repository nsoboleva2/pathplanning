#ifndef PARSER
#define PARSER

#include "tinyxml/tinystr.h"
#include "tinyxml/tinyxml.h"
#include "gl.const.h"
#include "grid.h"
#include "structures.h"
#include "openlist.h"
#include <unordered_map>
#include <sstream>
#include <cstring>
#include <iostream>
#include <cmath>


//Чтение карты
//функции обработки ошибок
inline void exit_error(const char *tag) {
    std::cout << "ERROR: Reading map failed. Not foung tag '" << tag <<  "'" << std::endl;
    exit(0);
}
inline void warning(const char *tag) {
    std::cout << "WARNING: Not foung tag '" << tag <<  "'. Using default value." << std::endl;
}

template <typename T>
int convert_to_int(T a) {
    std::istringstream myStream(a);
    int uintVar = 0;
    myStream >> uintVar;
    return uintVar;
}

template <typename T>
double convert_to_double(T a) {
    double var;
    var = std::stod(a);
    return var;
}

inline std::string get_info(TiXmlElement *b) {
    if(b) {
        const char *b_value = b->GetText();
        return b_value;
    }
    return "-1";
}

template<typename T>
TiXmlText * convert_to_xml(T a) {
    std::stringstream ss;
    ss << a;
    TiXmlText *text = new TiXmlText(ss.str().c_str());
    return text;
}

template<typename T>
std::string convert_to_string(T a) {
    std::stringstream so;
    so << a;
    std::string c = so.str();
    return c;
}

template<typename T>
const char * convert_to_char(T a) {
    std::ostringstream sc;
    sc << a;
    const char * c = sc.str().c_str();
    return c;
}

inline TiXmlText * convert_string(int * a, int len) {
    std::string st = "";
    for (int i = 0; i < len-1; i++)
        st += convert_to_string(a[i]) + ' ';
    st += convert_to_string(a[len - 1]);
    const char * c = st.c_str();
    TiXmlText *text = new TiXmlText(c);
    return text;
}

template<typename Search>
TiXmlText * convert_path(int * a, int len, Search &A, int row) {
    int buf[len];
    std::string st = "";
    for (int i = 0; i < len; ++i) buf[i] = a[i];
    for (auto next : A.path.path_lp) {
        if (next.point.y == row) buf[next.point.x] = 4;
    }
    for (int k = 0; k != len - 1; ++k) {
        if (buf[k] == 4) st += "* ";
        else st += convert_to_string(buf[k]) + " ";
    }
    if (buf[(len - 1)] == 4) st += "*";
    else st += convert_to_string(buf[len - 1]);
    const char * c = st.c_str();
    TiXmlText *text = new TiXmlText(c);
    return text;
}

inline TiXmlElement * create_step(size_t number, const OpenList &OPEN, const std::unordered_map<int, Node> &CLOSE) {
    TiXmlElement *step = new TiXmlElement(CNS_STEP);
    step->SetAttribute(CNS_NUMBER, number);
    TiXmlElement *open = new TiXmlElement(CNS_OPEN);
    for (auto next : OPEN.get_elements()) {
        TiXmlElement *node = new TiXmlElement(CNS_NODE);
        node->SetAttribute(CNS_X, next.point.x);
        node->SetAttribute(CNS_Y, next.point.y);
        node->SetDoubleAttribute(CNS_F, next.f);
        node->SetDoubleAttribute(CNS_G, next.g);
        if(next.parent != nullptr) {
            node->SetAttribute(CNS_PARENT_X, next.parent->point.x);
            node->SetAttribute(CNS_PARENT_Y, next.parent->point.y);
        }
        open->LinkEndChild(node);
    }
    step->LinkEndChild(open);
    TiXmlElement *close = new TiXmlElement(CNS_CLOSE);
    for (auto n : CLOSE) {
        Node next = n.second;
        TiXmlElement *node = new TiXmlElement(CNS_NODE);
        node->SetAttribute(CNS_X, next.point.x);
        node->SetAttribute(CNS_Y, next.point.y);
        node->SetDoubleAttribute(CNS_F, next.f);
        node->SetDoubleAttribute(CNS_G, next.g);
        if(next.parent != nullptr) {
            node->SetAttribute(CNS_PARENT_X, next.parent->point.x);
            node->SetAttribute(CNS_PARENT_Y, next.parent->point.y);
        }
        close->LinkEndChild(node);
    }
    step->LinkEndChild(close);
    return step;
}

template<typename Graph>
void get_map(Graph &graph, char *name) {
    TiXmlDocument doc(name);
    try {
        if(doc.LoadFile()) {
            std::cout << "Load sucsses" << std::endl;
        } else {
            throw "ERROR: Load failed. Try another file";
        }
    }
    catch (const char * exc) {
        std::cout << exc << std::endl;
        exit(0);
    }
    graph.filename = name;
    TiXmlElement *root = doc.FirstChildElement(CNS_ROOT);
    if(root) {
        TiXmlElement *map = root->FirstChildElement(CNS_MAP);
        if(map) {
            TiXmlElement *ttitle = map->FirstChildElement(CNS_TITLE);
            if(ttitle) {
                const char *title_value = ttitle->GetText();
                graph.title = title_value;
            }
            TiXmlElement *twidth = map->FirstChildElement(CNS_WIDTH);
            if(twidth) {
                const char *width_value = twidth->GetText();
                graph.width =  convert_to_int(width_value);
            } else exit_error(CNS_WIDTH);
            TiXmlElement *theight = map->FirstChildElement(CNS_HEIGHT);
            if(theight) {
                const char *height_value = theight->GetText();
                graph.height =  convert_to_int(height_value);
            } else exit_error(CNS_WIDTH);
            TiXmlElement *cellsize = map->FirstChildElement(CNS_CELLSIZE);
            if(cellsize) {
                const char *cellsize_value = cellsize->GetText();
                graph.cell_size =  convert_to_int(cellsize_value);
            } else {
                warning(CNS_CELLSIZE);
                graph.cell_size = 1;
            }
            TiXmlElement *startx = map->FirstChildElement(CNS_STARTX);
            if(startx) {
                const char *startx_value = startx->GetText();
                graph.start_cell.x = convert_to_int(startx_value);
            } else {
                warning(CNS_STARTX);
                graph.start_cell.x = 0;
            }
            TiXmlElement *starty = map->FirstChildElement(CNS_STARTY);
            if(starty) {
                const char *starty_value = starty->GetText();
                graph.start_cell.y = convert_to_int(starty_value);
            } else {
                warning(CNS_STARTY);
                graph.start_cell.y = 0;
            }
            TiXmlElement *finishx = map->FirstChildElement(CNS_FINISHX);
            if(finishx) {
                const char *finishx_value = finishx->GetText();
                graph.finish_cell.x =  convert_to_int(finishx_value);
            } else {
                warning(CNS_FINISHX);
                graph.finish_cell.x = 0;
            }
            TiXmlElement *finishy = map->FirstChildElement(CNS_FINISHY);
            if(finishy) {
                const char *finishy_value = finishy->GetText();
                graph.finish_cell.y =  convert_to_int(finishy_value);
            } else {
                warning(CNS_FINISHY);
                graph.finish_cell.y = 0;
            }
            graph.build_grid();
            TiXmlElement *g = map->FirstChildElement(CNS_GRID);
            if(g) {
                int i = 0;
                TiXmlElement *row = g->FirstChildElement(CNS_ROW);
                if (row) {
                    for (row; row; row = row->NextSiblingElement(CNS_ROW)) {
                        const char *number = row->Attribute(CNS_NUMBER);
                        if (number) i = convert_to_int(number);
                        const char *rowvalue = row->GetText();
                        int count = 0;
                        for (int j = 0; j < graph.width; ++j) {
                            int elem = rowvalue[count] - '0';
                            graph[i][j] = elem;
                            count += 2;
                        }
                        ++i;
                        if (i > graph.height) {
                            std::cout << "ERROR: the size of map does not match parametr" << std::endl;
                            exit(0);
                        }
                    }
                    if (i < graph.height) {
                        std::cout << "ERROR: the size of map does not match parametr" << std::endl;
                        exit(0);
                    }
                }
            } else exit_error(CNS_GRID);
            std::cout << "Reading map sucsses" << std::endl;
        } else exit_error(CNS_MAP);
        TiXmlElement *algorithm = root->FirstChildElement(CNS_ALGORITHM);
        if (algorithm) {
            TiXmlElement *searchtype = algorithm->FirstChildElement(CNS_SEARCHTYPE);
            if (searchtype) {
                graph.algorithm_info.searchtype = get_info(searchtype);
            } else exit_error(CNS_SEARCHTYPE);
            TiXmlElement * metrictype = algorithm->FirstChildElement(CNS_METRICTYPE);
            if (metrictype) {
                graph.algorithm_info.metrictype = get_info(metrictype);
            } else warning(CNS_METRICTYPE);
            TiXmlElement * hweight = algorithm->FirstChildElement(CNS_HWEIGHT);
            if (hweight) {
                const char *hweight_value = hweight->GetText();
                graph.algorithm_info.hweight = convert_to_double(hweight_value);
            } else {
                warning(CNS_HWEIGHT);
                graph.algorithm_info.hweight = 1;
            }
            TiXmlElement * breakingties = algorithm->FirstChildElement(CNS_BREAK);
            if (breakingties) {
                graph.algorithm_info.breakingties = get_info(breakingties);
            } else warning(CNS_BREAK);
            TiXmlElement * linecost = algorithm->FirstChildElement(CNS_LINE);
            if (linecost) {
                const char *linecost_value = linecost->GetText();
                graph.algorithm_info.linecost = convert_to_double(linecost_value);
            } else {
                warning(CNS_LINE);
                graph.algorithm_info.linecost = 1;
            }
            TiXmlElement * diagonalcost = algorithm->FirstChildElement(CNS_DIAGONAL);
            if (diagonalcost) {
                const char *diagonal_value = diagonalcost->GetText();
                graph.algorithm_info.diagonalcost = convert_to_double(diagonal_value);
            } else {
                warning(CNS_DIAGONAL);
                graph.algorithm_info.diagonalcost = sqrt(2);
            }

            TiXmlElement * allowdiagonal = algorithm->FirstChildElement(CNS_ALLOW);
            if (allowdiagonal) {
                const char *allowdiag_value = allowdiagonal->GetText();
                if (strstr(allowdiag_value, "true") != nullptr
                        || strstr(allowdiag_value, "1") != nullptr) {
                    graph.algorithm_info.allowdiagonal = true;
                }
                else graph.algorithm_info.allowdiagonal = false;
            } else {
                warning(CNS_ALLOW);
                graph.algorithm_info.allowdiagonal = false;
            }
            TiXmlElement * allowsqueeze = algorithm->FirstChildElement(CNS_ALLOWS);
            if (allowsqueeze) {
                const char *allowsq_value = allowsqueeze->GetText();
                if(strstr(allowsq_value,"true") != nullptr
                        || strstr(allowsq_value,"1") != nullptr) graph.algorithm_info.allowsqueeze = true;
                else graph.algorithm_info.allowsqueeze = false;
            } else {
                warning(CNS_ALLOWS);
                graph.algorithm_info.allowsqueeze = false;
            }
            TiXmlElement *cutcorners = algorithm->FirstChildElement(CNS_CUTCORNERS);
            if (cutcorners) {
                const char *cut_value = cutcorners->GetText();
                if(strstr(cut_value,"true") != nullptr
                        || strstr(cut_value, "1") != nullptr) graph.algorithm_info.cutcorners = true;
                else graph.algorithm_info.cutcorners = false;
            } else {
                warning(CNS_CUTCORNERS);
                graph.algorithm_info.cutcorners = false;
            }
        }

        TiXmlElement *options = root->FirstChildElement(CNS_OPTIONS);
        if(options) {

            TiXmlElement *loglevel = options->FirstChildElement(CNS_LOGLEVEL);
            if (loglevel) {
                graph.algorithm_info.loglevel = convert_to_double(get_info(loglevel));
            }

            TiXmlElement *logpath = options->FirstChildElement(CNS_LOGPATH);
            if (logpath) {
                if(logpath->GetText()) graph.algorithm_info.logpath = convert_to_string(get_info(logpath));
            }

            TiXmlElement *logfilename = options->FirstChildElement(CNS_LOGFILENAME);
            if (logfilename && logfilename->GetText()) {
                graph.algorithm_info.logfilename = convert_to_char(get_info(logfilename));
            } else {
                std::cout << "WARNING: Not foung tag '" << CNS_LOGFILENAME <<  "'. Using default value." << std::endl;
                std::stringstream ss;
                ss << name;
                std::string file_name = ss.str();
                if(file_name.rfind(".")) {
                    file_name.replace(file_name.rfind("."), 1, "_log.");
                } else {
                    file_name += "_log.xml";
                }
                graph.algorithm_info.logfilename = file_name;
            }
        }
    } else exit_error(CNS_ROOT);
}

//Функция вывода в XML
template<typename Graph, typename Search>
void create_xml(Graph &graph, Search &path) {
    TiXmlDocument doc;
    TiXmlDeclaration *decl = new TiXmlDeclaration("1.0","UTF-8","");
    doc.LinkEndChild(decl);

    TiXmlElement *root = new TiXmlElement(CNS_ROOT);

    doc.LinkEndChild(root);

    TiXmlElement *map = new TiXmlElement(CNS_MAP);
    TiXmlElement *twidth = new TiXmlElement(CNS_WIDTH);
    TiXmlText *textwidth = convert_to_xml(graph.width);
    twidth->LinkEndChild(textwidth);
    map->LinkEndChild(twidth);

    TiXmlElement *theight = new TiXmlElement(CNS_HEIGHT);
    TiXmlText *textheight = convert_to_xml(graph.height);
    theight->LinkEndChild(textheight);
    map->LinkEndChild(theight);


    TiXmlElement *cellsize = new TiXmlElement(CNS_CELLSIZE);
    TiXmlText *textcellsize = convert_to_xml(graph.cell_size);
    cellsize->LinkEndChild(textcellsize);
    map->LinkEndChild(cellsize);

    TiXmlElement *startx = new TiXmlElement(CNS_STARTX);
    TiXmlText *textstartx = convert_to_xml(graph.start_cell.x);
    startx->LinkEndChild(textstartx);
    map->LinkEndChild(startx);

    TiXmlElement *starty = new TiXmlElement(CNS_STARTY);
    TiXmlText *textstarty = convert_to_xml(graph.start_cell.y);
    starty->LinkEndChild(textstarty);
    map->LinkEndChild(starty);

    TiXmlElement *finishx = new TiXmlElement(CNS_FINISHX);
    TiXmlText *textfinishx = convert_to_xml(graph.finish_cell.x);
    finishx->LinkEndChild(textfinishx);
    map->LinkEndChild(finishx);

    TiXmlElement *finishy = new TiXmlElement(CNS_FINISHY);
    TiXmlText *textfinishy = convert_to_xml(graph.finish_cell.y);
    finishy->LinkEndChild(textfinishy);
    map->LinkEndChild(finishy);

    TiXmlElement *gr = new TiXmlElement(CNS_GRID);
    for (int i = 0; i < graph.height; ++i){
        TiXmlElement *row = new TiXmlElement(CNS_ROW);
        TiXmlText *textrow = convert_string(graph[i], graph.width);
        row->SetAttribute(CNS_NUMBER, i);
        row->LinkEndChild(textrow);
        gr->LinkEndChild(row);
    }
    map->LinkEndChild(gr);
    root->LinkEndChild(map);

    TiXmlElement *algorithm = new TiXmlElement(CNS_ALGORITHM);

    TiXmlElement *searchtype = new TiXmlElement(CNS_SEARCHTYPE);
    TiXmlText *texts = convert_to_xml(graph.algorithm_info.searchtype);
    searchtype->LinkEndChild(texts);
    algorithm->LinkEndChild(searchtype);

    TiXmlElement *metrictype = new TiXmlElement(CNS_METRICTYPE);
    TiXmlText *textm = convert_to_xml(graph.algorithm_info.metrictype);
    metrictype->LinkEndChild(textm);
    algorithm->LinkEndChild(metrictype);

    TiXmlElement *hweight = new TiXmlElement(CNS_HWEIGHT);
    TiXmlText *texth = convert_to_xml(graph.algorithm_info.hweight);
    hweight->LinkEndChild(texth);
    algorithm->LinkEndChild(hweight);

    TiXmlElement * breakingties = new TiXmlElement(CNS_BREAK);
    TiXmlText *textb = convert_to_xml(graph.algorithm_info.breakingties);
    breakingties->LinkEndChild(textb);
    algorithm->LinkEndChild(breakingties);

    TiXmlElement * linecost = new TiXmlElement(CNS_LINE);
    TiXmlText *textl = convert_to_xml(graph.algorithm_info.linecost);
    linecost->LinkEndChild(textl);
    algorithm->LinkEndChild(linecost);

    TiXmlElement * diagonalcost = new TiXmlElement(CNS_DIAGONAL);
    TiXmlText *textd = convert_to_xml(graph.algorithm_info.diagonalcost);
    diagonalcost->LinkEndChild(textd);
    algorithm->LinkEndChild(diagonalcost);

    const char * value;

    TiXmlElement * allowdiagonal = new TiXmlElement(CNS_ALLOW);
    if (graph.algorithm_info.allowdiagonal == true) value = "true";
    else value = "false";
    TiXmlText *texta = convert_to_xml(value);
    allowdiagonal->LinkEndChild(texta);
    algorithm->LinkEndChild(allowdiagonal);

    TiXmlElement * allowsqueeze = new TiXmlElement(CNS_ALLOWS);
    if (graph.algorithm_info.allowsqueeze == true) value = "true";
    else value = "false";
    TiXmlText *textas = convert_to_xml(value);
    allowsqueeze->LinkEndChild(textas);
    algorithm->LinkEndChild(allowsqueeze);

    TiXmlElement * cutcorners = new TiXmlElement(CNS_CUTCORNERS);
    if (graph.algorithm_info.cutcorners == true) value = "true";
    else value = "false";
    TiXmlText *textcuts = convert_to_xml(value);
    cutcorners->LinkEndChild(textcuts);
    algorithm->LinkEndChild(cutcorners);

    root->LinkEndChild(algorithm);

    TiXmlElement *options = new TiXmlElement(CNS_OPTIONS);
    TiXmlElement *loglevel = new TiXmlElement(CNS_LOGLEVEL);
    TiXmlText *textlo = convert_to_xml(graph.algorithm_info.loglevel);
    loglevel->LinkEndChild(textlo);
    options->LinkEndChild(loglevel);

    TiXmlElement *logpath = new TiXmlElement(CNS_LOGPATH);
    options->LinkEndChild(logpath);

    TiXmlElement *logfile = new TiXmlElement(CNS_LOGFILENAME);
    options->LinkEndChild(logfile);

    root->LinkEndChild(options);

    TiXmlElement *log = new TiXmlElement(CNS_LOG);
    TiXmlElement *mapfilename = new TiXmlElement(CNS_MAPFILENAME);
    TiXmlText *textmapfile = convert_to_xml(graph.filename);
    mapfilename->LinkEndChild(textmapfile);
    log->LinkEndChild(mapfilename);

    TiXmlElement *summary = new TiXmlElement(CNS_SUMMARY);
    summary->SetAttribute(CNS_NUMBEROFSTEPS, path.steps);
    summary->SetAttribute(CNS_NODES, path.nodes);
    summary->SetDoubleAttribute(CNS_LENGTH, path.length);
    summary->SetDoubleAttribute(CNS_TIME, path.time);
    log->LinkEndChild(summary);
    if (path.path_existance) {
        TiXmlElement *lpath = new TiXmlElement(CNS_PATH);
        for (int i = 0; i < graph.height; ++i){
            TiXmlElement *prow = new TiXmlElement(CNS_ROW);
            TiXmlText *textprow = convert_path<Search>(graph[i], graph.width, path, i);
            prow->SetAttribute(CNS_NUMBER, i);
            prow->LinkEndChild(textprow);
            lpath->LinkEndChild(prow);
        }

        log->LinkEndChild(lpath);

        TiXmlElement *lplevel = new TiXmlElement(CNS_LPLEVEL);
        int i = 0;
        for (auto next : path.path.path_lp) {
            TiXmlElement *node = new TiXmlElement(CNS_NODE);
            node->SetAttribute(CNS_X, next.point.x);
            node->SetAttribute(CNS_Y, next.point.y);
            node->SetAttribute(CNS_NUMBER, i);
            ++i;
            lplevel->LinkEndChild(node);
        }
        log->LinkEndChild(lplevel);

        TiXmlElement *hplevel = new TiXmlElement(CNS_HPLEVEL);
        int j = 0;
        for (auto next : path.path.path_hp) {
            TiXmlElement *section = new TiXmlElement(CNS_SECTION);
            section->SetAttribute(CNS_NUMBER, j);
            section->SetAttribute(CNS_START_X, next.start.point.x);
            section->SetAttribute(CNS_START_Y, next.start.point.y);
            section->SetAttribute(CNS_FINISH_X, next.finish.point.x);
            section->SetAttribute(CNS_FINISH_Y, next.finish.point.y);
            section->SetDoubleAttribute(CNS_LENGTH, next.length);
            ++j;
            hplevel->LinkEndChild(section);
        }
        log->LinkEndChild(hplevel);

        TiXmlElement *lowlevel = new TiXmlElement(CNS_LOWLEV);
        for (auto elem : path.lowlev) {
            lowlevel->LinkEndChild(elem);
        }
        log->LinkEndChild(lowlevel);

    } else {
        TiXmlText *nopath = convert_to_xml("PATH DOES NOT EXIST");
        root->LinkEndChild(nopath);
    }

    root->LinkEndChild(log);

    doc.SaveFile(graph.algorithm_info.logfilename.c_str());
    std::cout << "XML file has been created" << std::endl;
}

#endif // PARSER

