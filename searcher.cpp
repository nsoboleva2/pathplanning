#include "searcher.h"

inline int vertex(Cell item, int size) {
    return item.x * size + item.y;
}

Searcher::Searcher() { OPEN.open_size = 0; }
Searcher::~Searcher(void) {}

Result Searcher::find_path(const Graph &map, const Algorithm &info) {

    Node start_node = Node(map.start_cell);
    start_node.clear_state();
    start_node.h = heuristic(map.start_cell, map.finish_cell, map);
    start_node.f = map.algorithm_info.hweight * start_node.h;
    size_t close_size = 0;
    OPEN.resize(map.width);
    OPEN.put(start_node, info.breakingties);
    search_result = Result(false, 0, 0, 0);
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    while(!OPEN.empty()) {
        ++search_result.steps;
        //search_result.lowlev.push_back(create_step(search_result.steps, OPEN, CLOSE));
        Node current = OPEN.get(info.breakingties);
        CLOSE.insert({vertex(current.point, map.height), current});
        ++close_size;
        if (current.point == map.finish_cell) {
            end = std::chrono::system_clock::now();
            search_result.put(true, OPEN.open_size + close_size, current.g, start, end);
            search_result.path = reconstruct_path(current);
            return search_result;
        }
        auto parent = &(CLOSE.find(vertex(current.point, map.height))->second);
        get_successors(current, parent, map);
    }
    end = std::chrono::system_clock::now();
    search_result.put(false, OPEN.open_size + close_size, 0, start, end);
    return search_result;
}

double Searcher::get_cost(Node from, Node to, double linecost) const {
    return linecost * euclid_heuristic(from.point, to.point);
}

Node Searcher::reset_parent(Node &n, const Graph &map) {
    return n;
}

void Searcher::get_successors(Node current, Node * parent, const Graph &map) {
    for(auto elem : find_neighbors(current, parent, map)) {
        if(!CLOSE.count(vertex(elem.point, map.height))) {
            elem.g = current.g + get_cost(current, elem, map.algorithm_info.linecost);
            elem.h = heuristic(elem.point, map.finish_cell, map);
            elem.f = map.algorithm_info.hweight * elem.h + elem.g;
            elem = reset_parent(elem, map);
            OPEN.put(elem, map.algorithm_info.breakingties);
        }
    }
}

std::vector<Node> Searcher::find_neighbors(Node n, Node *parent, const Graph &map) const {
    std::vector<Node> result;
    int x = n.point.x;
    int y = n.point.y;
    if(map.algorithm_info.allowdiagonal) {
        for (int k = y - 1; k <= y + 1; ++k) {
            for (int l = x - 1; l <= x + 1; ++l) {
                if (!(k == y && l == x) && map.is_neighbor(Cell(l, k), n.point)) {
                    result.push_back(Node(Cell(l, k), parent));
                }
            }
        }
    } else {
        for (int k = x - 1; k <= x + 1; ++k)
            if (k != x && map.in_bounds(Cell(k, y)) && map.passable(Cell(k, y)))
                result.push_back(Node(Cell(k, y), parent));
        for (int l = y - 1; l <= y + 1; ++l)
            if (l != y && map.in_bounds(Cell(x, l)) && map.passable(Cell(x, l)))
                result.push_back(Node(Cell(x, l), parent));
    }
    return result;
}

void Searcher::get_primary_path(Path &path, Node goal) {
    Node current = goal;
    while (current.parent != nullptr) {
        path.path_lp.push_back(current);
        current = *current.parent;
    }
    path.path_lp.push_back(current);
    std::reverse(path.path_lp.begin(), path.path_lp.end());

}

void Searcher::get_secondary_path(Path &path) {
    std::vector<Node>::const_iterator it = path.path_lp.begin();
    Cell curr, next, move;
    Node start = *it;
    while (it != --path.path_lp.end()) {
        curr = it->point;
        ++it;
        next = it->point;
        move = Cell(next.x - curr.x, next.y - curr.y);
        ++it;
        if ((it->point.x - next.x) != move.x || (it->point.y - next.y) != move.y) {
            --it;
            path.path_hp.push_back(Section(start, *it));
            start = *it;
        } else { --it; }
    }
}


Path Searcher::reconstruct_path(Node goal) {
    Path result;
    get_primary_path(result, goal);
    get_secondary_path(result);
    return result;

}
